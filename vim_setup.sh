#!/bin/bash -e
 
#Install LLVM 12
echo "Installing LLVM-12..."
echo ""
echo ""
wget https://apt.llvm.org/llvm.sh
chmod +x llvm.sh
sudo ./llvm.sh 12
rm llvm.sh
echo ""
echo ""
echo "DONE"
echo ""
echo ""
 
# Setup clang-12 as default compiler
echo "Setting up clang-12 as default compiler..."
echo ""
echo""
sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-12 90
sudo update-alternatives --install /usr/bin/lldb lldb /usr/bin/lldb-12 90
sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-12 90
sudo update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-12 90
sudo update-alternatives --install /usr/bin/cc cc /usr/bin/clang-12 90
sudo update-alternatives --install /usr/bin/clangd clangd /usr/bin/clangd-12 90
sudo update-alternatives --install /usr/bin/ld ld /usr/bin/x86_64-linux-gnu-ld 10
sudo update-alternatives --install /usr/bin/ld ld /usr/bin/lld-12 12
echo ""
echo ""
echo "DONE"
echo ""
echo ""
 
#Setup nvim dependencies
echo "Installing cmake, curl, node, python3..."
echo ""
echo ""
sudo apt -y install cmake curl python3 python3-pip python3-nose
sudo python3 -m pip install pynvim
curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -
sudo apt update
sudo apt -y install nodejs
echo ""
echo ""
echo "Done"
echo ""
echo ""
 
#Setup nvim
echo "Setting up nvim..."
echo ""
echo ""
mkdir -p ${HOME}/.config/nvim_config
cd $_
mkdir -p coc
git clone https://gitlab.com/mirza.sakic/nvim.git
cd nvim
mkdir -p ${HOME}/.local/
mv nvim04 $_
mkdir -p ${HOME}/.local/bin
chmod +x neo
mv neo ${HOME}/.local/bin
cd ${HOME}
PATH=${HOME}/.local/nvim04/bin/:${PATH} XDG_CONFIG_HOME=${HOME}/.config/nvim_config nvim --headless +PlugInstall +qall
PATH=${HOME}/.local/nvim04/bin:${PATH} XDG_CONFIG_HOME=${HOME}/.config/nvim_config timeout 30s nvim --headless +"CocInstall coc-snippets coc-clangd"; exit 0
