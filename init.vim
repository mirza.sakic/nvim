if !empty($XDG_CONFIG_HOME) 
  let g:editor_root=$XDG_CONFIG_HOME . "/nvim"
else
  if has('nvim')
    let g:editor_root=expand("~/.config/nvim")
  else
    let g:editor_root=expand("~/.vim")
  endif
endif
" Plugins {{{
call plug#begin(g:editor_root . '/plugged')
Plug 'tomtom/tcomment_vim'
Plug 'ap/vim-buftabline'
Plug 'tpope/vim-sensible'
Plug 'sjl/gundo.vim'
"Plug 'NLKNguyen/papercolor-theme', {'tag': 'v0.9'}
Plug 'rakr/vim-one'
Plug 'honza/vim-snippets'
Plug 'SirVer/ultisnips'
Plug 'junegunn/vim-easy-align'
"Plug 'nathanaelkane/vim-indent-guides'
Plug 'mhinz/vim-signify'
Plug 'mileszs/ack.vim'
"Plug 'osyo-manga/vim-over'
Plug 'schickling/vim-bufonly'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
"Plug 'Yggdroot/indentLine'
Plug 'tpope/vim-dispatch'
Plug 'honza/dockerfile.vim'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'sheerun/vim-polyglot'
Plug 'rhysd/vim-clang-format'
Plug 'terryma/vim-multiple-cursors'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }
Plug 'junegunn/fzf.vim'
Plug 'vim-scripts/gtags.vim'
" Plug 'jsfaint/gen_tags.vim'
Plug 'igankevich/mesonic'
Plug 'rdnetto/YCM-Generator', {'branch': 'develop'}
"Plug 'semirpuskarevic/YCM-Generator', { 'branch': 'meson' }
Plug 'vhdirk/vim-cmake'
"Plug 'majutsushi/tagbar'
Plug 'itchyny/lightline.vim'
Plug 'oblitum/rainbow'
"Plug 'uplus/vim-clang-rename'
" Plug 'haya14busa/incsearch.vim'
" Plug 'haya14busa/incsearch-easymotion.vim'
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'tpope/vim-eunuch'
"Plug 'Valloric/YouCompleteMe'
Plug 'neomake/neomake'
"Plug 'zxqfl/tabnine-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'jackguo380/vim-lsp-cxx-highlight'
"Plug 'Shougo/echodoc.vim'
Plug 'richq/vim-cmake-completion'
Plug 'idanarye/vim-vebugger'
"Plug 'tpope/vim-unimpaired'
Plug 'rakr/vim-one'
Plug 'ndmitchell/ghcid', { 'rtp': 'plugins/nvim' }
Plug 'machakann/vim-highlightedyank'
call plug#end()
" }}}
" Basic Settings {{{
syntax on
filetype plugin indent on
let mapleader=' '       " leader is space 
set tabstop=2       " number of visual spaces per TAB
set softtabstop=2   " number of spaces in tab when editing
set shiftwidth=2
set expandtab       " tabs are spaces
set shiftround    " use multiple of shiftwidth when indenting with '<' and '>'
set cursorline          " highlight current line
set showmatch           " highlight matching [{()}]
set modeline
set rnu              " show line numbers
set modelines=1
set mouse=a
set lazyredraw
set colorcolumn=80              "Highlight the character limit
set nowrap       "Don't wrap lines
set linebreak    "Wrap lines at convenient points
set wildignorecase
set showtabline=2
" Custom Functions {{{

fun! s:switchBetweenHeaderAndImplementation()
  if match(expand('%'), '\.\(c\|cc\|cpp\)$') > 0
    let target = 'header'
    let search_pattern = substitute(expand('%:t'), '\.c\(.*\)$', '.h*', '')
  elseif match(expand('%'), '\.\(h\|hpp\)$') > 0
    let target = 'implementation'
    let search_pattern = substitute(expand('%:t'), '\.h\(.*\)$', '.c*', '')
  else
    echo 'Failed to switch to header or implementation for this file'
    retu
  endif
  let dir_name = fnamemodify(expand('%:p'), ':h')
  let cmd = 'find ' . dir_name .  ' . -type f ' .
        \ '-iname ' . search_pattern . ' -print -quit'
  let file_name = substitute(system(cmd), '\n$', '', '')
  if filereadable(file_name)
    exe 'e ' file_name
  else
    echo 'No ' . target . ' file found for ' . expand('%:t')
  endif
endf

nnoremap <silent> <leader>o :call <SID>switchBetweenHeaderAndImplementation()<CR>

" toggle between number and relativenumber
function! ToggleNumber()
  if(&relativenumber == 1)
    set norelativenumber
    set number
  else
    set relativenumber
  endif
endfunc
 
" strips trailing whitespace at the end of files. this
" is called on buffer write in the autogroup above.
function! <SID>StripTrailingWhitespaces()
  " save last search & cursor position
  let _s=@/
  let l = line(".")
  let c = col(".")
  %s/\s\+$//e
  let @/=_s
  call cursor(l, c)
endfunction

function! s:do_chrome_refresh()
  let l:com = "
        \ndow_id=$(xdotool getactivewindow);
        \xdotool search --onlyvisible  --class 'Chrome' windowfocus key F5; 
        \xdotool windowfocus $ndow_id"
  call system(l:com)
endfunction

function! s:elm_dispatch_chrome()
  let l:win_id = system("xdotool getactivewindow")
  if !exists("g:elm_reactor_enabled")
    execute "Dispatch! elm-reactor"
    autocmd BufWritePost *.elm DOChromeRefresh
    let g:elm_reactor_enabled = 1
  endif
  execute "Dispatch! google-chrome http://localhost:8000/" . expand("%:t")
  call system("sleep 2")
  call system("xdotool windowfocus " . l:win_id)
endfunction
command! EChrome call <sid>elm_dispatch_chrome()
command! DOChromeRefresh call <sid>do_chrome_refresh()
 " }}}
" Scrolling {{{
set scrolloff=8 
set sidescrolloff=15
set sidescroll=1
" }}}
" Colorscheme {{{
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
if &t_Co >= 256 || has("gui_running")
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  set background=dark
  try
    colorscheme one " awesome colorscheme
  catch
    colorscheme delek  
  endtry
endif
" }}}
" Search and highlight {{{
" turn off search highlight
set hlsearch
nnoremap <leader>nh :nohlsearch<CR>
" highlight last inserted text
nnoremap gV `[v`]
" }}}
"Backup {{{
" set backup
" set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
" set backupskip=/tmp/*,/private/tmp/*
" set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
" set writebackup
" }}}
" Persistent Undo {{{
set undodir=~/.undodir
set undofile
set undolevels=1000 "maximum number of set changes that can be undone
set undoreload=10000 "maximum number lines to save for undo on a buffer reload
" }}}
" Merlin Ocaml {{{
try
  let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
  execute "set rtp+=" . g:opamshare . "/merlin/vim"
catch
endtry
" }}}
" }}}
" Plugin Settings {{{
" CPP Highlight {{{
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_posix_standard = 1
let g:cpp_experimental_simple_template_highlight = 1
let g:cpp_concepts_highlight = 1
" }}}
" EasyAlign {{{
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" }}}
" Vim plugins {{{
" YCM {{{
"let g:ycm_key_list_previous_completion = []
"let g:ycm_key_list_select_completion = []
let g:ycm_autoclose_preview_window_after_completion = 0
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_confirm_extra_conf = 0
" let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
nnoremap <leader>jd :YcmCompleter GoTo<CR>
"let g:ycm_add_preview_to_completeopt=1
let g:ycm_auto_trigger=1
"set completeopt+=preview
let g:ycm_show_diagnostics_ui = 1
let g:syntastic_error_symbol = "✗"
let g:syntastic_warning_symbol = "⚑"
"}}}
" }}}
" vim-buftbaline {{{
set hidden
nmap <esc><tab> :bnext<CR>
nmap <esc><bs> :bprev<CR>
" }}}
" Ultisnips {{{
let g:UltiSnipsEditSplit = 'vertical'
let g:UltiSnipsExpandTrigger="<c-j>"
" }}}
" Incsearch {{{
set viminfo='100,f1  "Save up to 100 marks, enable capital marks
" let g:incsearch#auto_nohlsearch = 1
" map /  <Plug>(incsearch-easymotion-/)
" map ?  <Plug>(incsearch-easymotion-?)
" map g/ <Plug>(incsearch-easymotion-stay)
" map n  <Plug>(incsearch-nohl-n)
" map N  <Plug>(incsearch-nohl-N)
" map *  <Plug>(incsearch-nohl-*)
" map #  <Plug>(incsearch-nohl-#)
" map g* <Plug>(incsearch-nohl-g*)
" map g# <Plug>(incsearch-nohl-g#)
" }}}
" Clang Format {{{
let g:clang_format#style_options = {
      \ "AccessModifierOffset" : -4,
      \ "AllowShortIfStatementsOnASingleLine" : "true",
      \ "AlwaysBreakTemplateDeclarations" : "true",
      \ "Standard" : "C++11"}
" map to <Leader>cf in C++ code
let g:clang_format#command="clang-format"
autocmd FileType c,cpp,objc nnoremap <buffer><Leader>cf :<C-u>ClangFormat<CR>
autocmd FileType c,cpp,objc vnoremap <buffer><Leader>cf :ClangFormat<CR>
function FormatBuffer()
  if &modified && !empty(findfile('.clang-format', expand('%:p:h') . ';'))
    let cursor_pos = getpos('.')
    :%!clang-format
    call setpos('.', cursor_pos)
  endif
endfunction

autocmd BufWritePre *.h,*.hpp,*.c,*.cpp,*.hxx :call FormatBuffer()
" }}}
" fzf-vim {{{
"nnoremap <silent> <leader>f :call FzyCommand('rg --files .', ':e ')<cr>
"nnoremap <leader>f :call FzyCommand("find . -type f", ":e")<cr>
nnoremap <leader>b :Buffers<cr>
nnoremap <leader>c :BCommits<CR>
" }}}
" Lightline {{{
let g:lightline = {
      \ 'colorscheme': 'one',
      \ 'active': {
      \ 'left': [ [ 'mode', 'paste' ], [ 'fugitive', 'cocstatus' ] ],
      \ 'right': [ [ 'lineinfo' ], [ 'percent' ], [ 'filetype' ] ]
      \ },
      \ 'component_function': {
      \   'modified': 'LightLineModified',
      \   'readonly': 'LightLineReadonly',
      \   'fugitive': 'LightLineFugitive',
      \   'filename': 'LightLineFilename',
      \   'fileformat': 'LightLineFileformat',
      \   'filetype': 'LightLineFiletype',
      \   'fileencoding': 'LightLineFileencoding',
      \   'mode': 'LightLineMode',
      \   'cocstatus': 'coc#status',
      \ },
      \ 'separator': { 'left': '', 'right': '' },
      \ 'subseparator': { 'left': '', 'right': '' }
      \ }

autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()

function! LightLineModified()
  return &ft =~ 'help\|vimfiler\|gundo' ? '' : &modified ? '+' : &modifiable ? '' : '-'
endfunction

function! LightLineReadonly()
  return &ft !~? 'help\|vimfiler\|gundo' && &readonly ? '' : ''
endfunction

function! LightLineFilename()
  let fname = expand('%:t')
  return fname == '__Tagbar__' ? g:lightline.fname :
        \ fname =~ '__Gundo\|NERD_tree' ? '' :
        \ ('' != LightLineReadonly() ? LightLineReadonly() . ' ' : '') .
        \ ('' != fname ? fname : '[No Name]') .
        \ ('' != LightLineModified() ? ' ' . LightLineModified() : '')
endfunction


let g:tagbar_status_func = 'TagbarStatusFunc'

function! TagbarStatusFunc(current, sort, fname, ...) abort
  let g:lightline.fname = a:fname
  return lightline#statusline(0)
endfunction

function! LightLineFugitive()
  if &ft !~? 'vimfiler\|gundo' && exists("*fugitive#head")
    let _ = fugitive#head()
    return strlen(_) ? ' '._ : ''
  endif
  return ''
endfunction

function! LightLineFileformat()
  return winwidth(0) > 70 ? &fileformat : ''
endfunction

function! LightLineFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype : 'no ft') : ''
endfunction

function! LightLineFileencoding()
  return winwidth(0) > 70 ? (strlen(&fenc) ? &fenc : &enc) : ''
endfunction

function! LightLineMode()
  let fname = expand('%:t')
  return fname == '__Tagbar__' ? 'Tagbar' :
        \ fname == '__Gundo__' ? 'Gundo' :
        \ fname == '__Gundo_Preview__' ? 'Gundo Preview' :
        \ winwidth(0) > 60 ? lightline#mode() : ''
endfunction


" }}}
" Signify {{{
let g:signify_vcs_list = ['git']
let g:signify_sign_add = '⨁'
let g:signify_sign_delete = '⨴'
let g:signify_sign_delete_first_line = '⨂'
let g:signify_sign_change = '⨸'
let g:signify_sign_changedelete = '⨂'
" }}}
" ACK {{{
if executable('rg')
  let g:ackprg = 'rg --vimgrep --no-heading'
endif
" }}}
" GTags {{{
"
nnoremap <Leader>gg :Gtags
nnoremap <Leader>gn :cn<CR>
nnoremap <Leader>gp :cp<CR>
nnoremap <Leader>gj :GtagsCursor<CR>
nnoremap <Leader>gi :Gtags -f %<CR>

" }}}
" Mproject {{{
autocmd FileType cpp nnoremap <silent> <leader>tt :MProjectDispatchTests<CR>
autocmd FileType cpp nnoremap <silent> <leader>ts :MProjectSelectTestExe<CR>
autocmd FileType cpp nnoremap <silent> <leader>tc :MProjectSelectTestCase<CR>
" }}}
" Multiple Cursors {{{
" Called once right before you start selecting multiple cursors
function! Multiple_cursors_before()
  let g:deoplete#disable_auto_complete = 1
endfunction
function! Multiple_cursors_after()
  let g:deoplete#disable_auto_complete = 0
endfunction
let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_next_key='<c-i>'
let g:multi_cursor_prev_key='<c-k>'
let g:multi_cursor_skip_key='<c-x>'
let g:multi_cursor_select_all_key='<c-s>'
let g:multi_cursor_select_all_word_key = '<c-a>'
let g:multi_cursor_quit_key='<Esc>'

" }}} 
" ElmCast {{{
let g:elm_format_autosave = 1
let g:elm_detailed_complete = 1
" let g:ycm_semantic_triggers = {
"      \ 'elm' : ['.'],
"      \}
" }}}
" NeoMake {{{
call neomake#configure#automake('nrwi', 200)
" }}}
" GHCID {{{
let g:ghcid_command="stack exec -- ghcid"
" }}}
" COC{{{
" TextEdit might fail if hidden is not set.
set hidden

let g:coc_disable_startup_warning = 1

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup


" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>nf  <Plug>(coc-format-selected)
nmap <leader>nf  <Plug>(coc-format-selected)

nnoremap <silent> <leader>f :Files<cr>
nnoremap <silent> <leader>F :FZF ~<cr>



augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of LS, ex: coc-tsserver
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings using CoCList:
" Show all diagnostics.
" nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" " Manage extensions.
" nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" " Show commands.
" nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" " Find symbol of current document.
" nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" " Search workspace symbols.
" nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" " Do default action for next item.
" nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" " Do default action for previous item.
" nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" " Resume latest coc list.
" nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
"
"COC snippets
" Use <C-l> for trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand)

" Use <C-j> for select text for visual placeholder of snippet.
vmap <C-j> <Plug>(coc-snippets-select)

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'

" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'

" Use <C-j> for both expand and jump (make expand higher priority.)
imap <C-j> <Plug>(coc-snippets-expand-jump)

" inoremap <silent><expr> <TAB>
"       \ pumvisible() ? coc#_select_confirm() :
"       \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_snippet_next = '<tab>'

" }}}
" }}}
  " Buffer Settings {{{
  " meson {{{
  augroup meson
    autocmd!
    autocmd BufWritePost meson.build MProjectUpdateConfig
  augroup END
  " }}}
  " c {{{
  augroup c
	  autocmd!
	  autocmd FileType c nnoremap <silent> <buffer> <leader>dd :call ClangGotoDeclaration()<CR><Esc>
	  autocmd FileType c nnoremap <silent> <buffer> <leader>dp :call ClangGotoDeclarationPreview()<CR><Esc>
    " autocmd BufWritePost * Neomake!
	  " autocmd BufRead,BufNewFile *.h set filetype=c
  augroup END
  " }}}
  " vim {{{
  augroup vim
	  autocmd!
	  autocmd FileType vim setlocal foldmethod=marker
  augroup END
  " }}}
  " }}}
" Testing {{{

let g:cppproject#build_dir_name = "build"

function! s:find_test_exe()
  return split(system("find $(pwd) -executable -exec file {} \\; | grep ELF | cut -d':' -f1"),'\n')
endfunction

function! s:n_mpinit()
  execute "Dispatch :MesonInit"
  call system("cp $HOME/.ycm_extra_conf.py $(pwd)")
endfunction

function! s:find_test_exe_c(e)
  let g:cppproject#test_file_exe = a:e
  execute "MProjectSelectTestCase"
	call feedkeys('i')
endfunction

function! s:find_test_case()
  let l:fzf_filter_strings = ["*;*"]
  let l:vec = split(system(g:cppproject#test_file_exe . " " . "--gtest_list_tests"),"\n")
  let l:case_name = ""
  for l:var in l:vec
    if l:var[0] != " "
      let l:case_name = l:var[:-2] . ";"
      call add(l:fzf_filter_strings,l:case_name . "*")
    else
      call add(l:fzf_filter_strings,l:case_name . matchstr(l:var,'\w\+'))
    endif
  endfor
  return l:fzf_filter_strings
endfunction

function! s:find_test_case_c(e)
  let l:case_name = split(a:e,";")
  let g:cppproject#test_to_run = l:case_name
endfunction

function! s:run_tests()
  if !exists('g:cppproject#project_root_path')
    execute "echo 'Configure the project first!'"
    return
  endif
  if !exists('g:cppproject#test_file_exe')
    execute "echo 'Select a test exe file!'"
    return
  endif
  execute "Make"
  execute "echo 'Running tests!'"
  execute 'lcd ' . g:cppproject#project_root_path . "/" . g:cppproject#build_dir_name
  let l:makeprg=&makeprg
  let l:errorformat=&errorformat
  let &makeprg=g:cppproject#test_file_exe
  set errorformat=%f:%l:\ %m
  silent execute "Make" . "'--gtest_filter=" . join(g:cppproject#test_to_run,".") . "'"
  let &makeprg=l:makeprg
  let &errorformat=l:errorformat
endfunction

function! s:project_generate_config()
  if !exists('g:cppproject#project_root_path')
    let g:cppproject#project_root_path = getcwd()
    execute "MesonInit"
    execute "YcmGenerateConfig"
  endif
endfunction

function! s:project_update_config()
  if exists('g:cppproject#project_root_path')
    execute 'lcd ' . g:cppproject#project_root_path
    execute "YcmGenerateConfig"
  endif
endfunction

function! s:project_select_files()
  if exists('g:cppproject#project_root_path')
    execute "lcd " . g:cppproject#project_root_path
  endif
  execute "Files"
endfunction


command!  MProjectSelectTestExe call fzf#run({
      \ 'source': <sid>find_test_exe(), 
      \ 'sink': function('<sid>find_test_exe_c'),
      \ 'options': '+m',
      \ 'down': len(<sid>find_test_exe()) + 2
      \})

command! MProjectSelectTestCase call fzf#run({
      \ 'source': <sid>find_test_case(), 
      \ 'sink': function('<sid>find_test_case_c'),
      \ 'options': '+m',
      \ 'down': len(<sid>find_test_case()) + 2
      \})

command! MProjectDispatchTests call <sid>run_tests()
command! MProjectFileSelector call <sid>project_select_files()
command! MPInit call <sid>n_mpinit()
"command! NMPInit call <sid>n_mpinit()
command! MProjectUpdateConfig call <sid>project_update_config()
" "}}}
  " General Mappings and Functions {{{
" misc {{{
inoremap ,, <esc>
:map <F5> :setlocal spell! spelllang=en_us<CR>
" hide buffer
nnoremap <leader>h :hide<cr>
" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>rW mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm
" cd to change to open directory.
map <leader>cd :cd %:p:h<cr>
" no / /\v

" }}}
" cursor movement windows {{{
nnoremap <C-h> <C-W>h
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-l> <C-W>l
" move vertically by visual line
nnoremap j gj
nnoremap k gk
nnoremap 0 g0
nnoremap $ g$
" }}}
" terminal {{{
if exists(':tnoremap')
  tnoremap <C-h> <C-\><C-n><C-w>h
  tnoremap <C-j> <C-\><C-n><C-w>j
  tnoremap <C-k> <C-\><C-n><C-w>k
  tnoremap <C-l> <C-\><C-n><C-w>l
  tnoremap <esc><esc> <C-\><C-n>
  tnoremap <esc><tab> <C-\><C-n>:bnext<CR>
  tnoremap <esc><bs> <C-\><C-n>:bprev<CR>
  tnoremap <c-l> clear<CR>
  autocmd BufWinEnter,WinEnter term://* startinsert
  autocmd BufLeave term://* stopinsert
endif
" }}}
" dimensions {{{
nnoremap <leader>- <c-w>_ " Max height
nnoremap <leader>\ <c-w><bar> " Max width leader+|
nnoremap <leader>= <c-w>= " Fair defaults
" }}}
" toggle gundo {{{
nnoremap <leader>tU :GundoToggle<CR>
nnoremap <leader>tN :call ToggleNumber()<CR>
nnoremap <leader>rR :Dispatch! "rdm"<CR>
"}}}
" edit vimrc/bashrc and load vimrc bindings {{{
nnoremap <leader>eV :e $MYVIMRC<CR>
nnoremap <leader>eB :e ~/.bashrc<CR>
nnoremap <leader>sV :source $MYVIMRC<CR>
set mouse=a
" set completeopt=longest,menuone
set completeopt=longest,menuone
" }}}
" Move visual block {{{
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
" }}}
" Clipboard {{{
vmap <leader>y "+y
vmap <leader>d "+d
nmap <leader>p "+p
nmap <leader>P "+P
vmap <leader>p "+p
vmap <leader>P "+P
vmap <leader>r "_dP       // Replace the selected text with what's in the yank register
" }}}
" Search and highlight {{{
" turn off search highlight
" highlight last inserted text
" }}}
"}}}
